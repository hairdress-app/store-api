import Vuex from 'vuex'
import axios from 'axios'

const createStore = () => {
  return new Vuex.Store({
    state: {
      counter: 1,
      currentCategory: { name: 'All' },
      // fed by nuxtServerInit
      categories: {},
      products: {}
    },
    mutations: {
      changeCategory (state, category) {
        state.currentCategory = category
      }
    },
    getters: {
      productsByCategory: state => category => {
        console.log(category.id)
        if (category.id === 0) {
          return state.products
        } else {
          return state.products.filter(product => product.category_id == category.id)
        }
      }
    },
    actions: {
      async nuxtServerInit ({ commit }, { req }) {
        let baseURL = (process.env.NODE_ENV === 'production') ? 'https://online-demo-store.herokuapp.com' : 'http://localhost:3000'

        // get all the products from the db
        {
          let { data } = await axios.get(baseURL + '/api/products/all')
          this.state.products = data
        }
        // get all the categories from the db
        {
          let { data } = await axios.get(baseURL + '/api/categories')
          this.state.categories = data
        }
      }
    }
  })
}

export default createStore
