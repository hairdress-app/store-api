var promise = require('bluebird');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);

var connectionString; 
if (process.env.NODE_ENV !== 'production')  
/* local db */
    connectionString = 'postgres://postgres:+-8246dd@localhost:5432/online-store';
else
/* heroku db */
    connectionString = 'postgres://utnsgthiqnlqnd:01054f2d79b4ff388dc22498e83bd0156cd9a2b2bc214d4c467ffd05d08f6427@ec2-54-217-214-201.eu-west-1.compute.amazonaws.com:5432/d2i0rcc0er339h';

var db = pgp(connectionString);

module.exports = db;