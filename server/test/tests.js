'use strict';
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);

const server = require('../bin/www');
const app = require('../app')

describe('Status and content', function() {
    describe ('Main page', function() {
        it('status and content', function(done) {
            chai.request(app)
                .get('/')
                .end( function(err, res) {
                    res.should.not.be.undefined;
                    res.statusCode.should.be.eq(200);
                    done();
                });
        });
    })
});

let token; // will have admin access
let token2; // will not have admin access

describe ('API endpoint api/users', function() {
    /* JWT for this set of tests
        it might get updated at a later AUTH*/
    // Used to POST, compare GETS and later AUTHs
    const userJSON = {
        "first_name": "diego ",
        "last_name": " cardenas",
        "username": " 1dgod1@hotmail.com " ,
        "passwrd": "123244114145",
        "user_type_id": "0"
    };
    //this.timeout(5000);
    describe ('POSTS', function() {
        /* POST api/users */
        it('Register an user succesfully', function(done) {
            chai.request(app)
                .post('/api/users')
                .send(userJSON)
                .end( function(err, res) {
                    if (err) {
                        console.log(err.message);
                        done(err);
                    }
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.ownProperty('token');
                    // Save the token for later tests
                    token = res.body.token;
                    //console.log(token);
                    done();
                });
        });
        it('Tries to re-POST the same user', function(done) {
            chai.request(app)
                .post('/api/users')
                .send(userJSON)
                .end( function(err, res) {
                    res.should.have.status(422);
                    res.should.be.json;
                    done();
                });
        });
        it('Tries to post an user without the last_name field', function (done) {
            chai.request(app)
                .post('/api/users')
                .send({
                    "first_name": "diego ",
                    "username": " 1411111131@gotmail.com " ,
                    "passwrd": "123244114145",
                    "user_type_id": "1"
                })
                .end( function(err, res) {
                    res.should.have.status(400);
                    res.should.be.json;
                    done();
                });
        });
    });
    describe('GETS', function () {
        /* GET api/users/all */
        it('GETs all users', function(done) {
            chai.request(app)
                .get('/api/users/all')
                .set('x-access-token', token)
                .end( function(err, res) {
                    res.should.not.be.undefined;
                    res.statusCode.should.be.eq(200);
                    //console.log(res.body);
                    let user = res.body[0];
                    user.id.should.be.a('number');
                    user.first_name.should.be.a('string');
                    user.last_name.should.be.a('string');
                    user.username.should.be.a('string');
                    new Date(user.created).should.be.a('date');
                    done();
                }
            );
        });
        // GET api/users/all
        it('Tries to GET all users with no admin rights', function (done) {
            const userJSON2 = {
                "first_name": "diego ",
                "last_name": " cardenas",
                "username": " tripto@1hotmail.com " ,
                "passwrd": "123244114145",
                "user_type_id": "1"
            };
            chai.request(app)
            .post('/api/users')
            .send(userJSON2)
            .end( function(err, res) {
                if (err) done(err);
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.ownProperty('token');
                token2 = res.body.token;
                // We make the GET all request with the newly registered user token
                chai.request(app)
                .get('/api/users/all')
                .set('x-access-token', token2)
                .end( function(error, response) {
                    response.should.not.be.undefined;
                    response.statusCode.should.be.eq(401);
                    // finally we delete the user
                    chai.request(app)
                    .delete('/api/users')
                    .set('x-access-token', token2)
                    .end( function(err2, res2) {
                        if(err2) done(err2);
                        res2.should.have.status(200);
                        res2.should.be.json;
                        done();
                    });
                    //console.log(token);
                });
            });
        })
        /* GET api/users/ */
        it('GETs an user', function(done) {
            chai.request(app)
                .get('/api/users')
                .set('x-access-token', token)
                .end( function(err, res) {
                    res.should.not.be.undefined;
                    res.statusCode.should.be.eq(200);
                    //console.log(token);
                    let user = res.body.data;
                    user.id.should.be.a('number');
                    user.first_name.should.be.a('string');
                    user.last_name.should.be.a('string');
                    user.username.should.be.a('string');
                    new Date(user.created).should.be.a('date');
                    done();
                });
        });
        /* GET api/users */
        it('Tries to GET an user with an invalid token', function(done) {
            chai.request(app)
                .get('/api/users')
                .set('x-access-token', "invalid token")
                .end( function(err, res) {
                    res.should.have.status(401);
                    res.should.be.json;
                    done();
                });
        });
    })
    describe ('AUTHs', function() {
    
        it('Auth with a non-registered username', function (done) {
            chai.request(app)
                .post('/api/users/auth')
                .send({
                    "username": "banana@split.net",
                    "passwrd": "youwouldneverguessthispassword123"
                })
                .end( function(err, res) {
                    res.should.have.status(401);
                    res.should.be.json;
                    done();
                });
        });
        it('Auth with an invalid password', function (done) {
            chai.request(app)
                .post('/api/users/auth')
                .send({
                    "username": userJSON.username,
                    "passwrd": "incorrectpassword69"
                })
                .end( function(err, res) {
                    res.should.have.status(401);
                    res.should.be.json;
                    done();
                });
        });
        it('Auth with a valid username and password', function (done) {
            chai.request(app)
                .post('/api/users/auth')
                .send({
                    "username": userJSON.username,
                    "passwrd": userJSON.passwrd
                })
                .end( function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.ownProperty('token');
                    // Save the token for later tests
                    token = res.body.token;
                    done();
                });
        });
    });
    describe('PUTS', function () {
        it('Tries to update an user with an empty token', function (done) {
            chai.request(app)
                .put('/api/users')
                .set('x-access-token', "")                
                .send({
                    "first_name": "diego ",
                    "last_name": " cardenas",
                    "passwrd": "123244114145"
                })
                .end(function(error, response){
                  response.should.have.status(400);
                  response.should.be.json;
                  done();
                });
        });
        it('Tries to update an user with an invalid token', function (done) {
            chai.request(app)
                .put('/api/users')
                // invalid token
                .set('x-access-token', "eyJhbGciOiJIUzI1NiIsInR5cCI7IkpXVCJ9.eyJpZCI6MTMxLCJpYXQiOjE1MTY3MzgwNDEsImV4cCI6MTUxNjgyNDQ0MX0.TxZH49cpmaPrsBHZzKGAe_rmAkc9-tC5exuuOYq4138")                
                .send({
                    "first_name": "diego ",
                    "last_name": " cardenas",
                    "passwrd": "123244114145"
                })
                .end(function(error, response){
                  response.should.have.status(401);
                  response.should.be.json;
                  done();
                });
        });
        it('Tries to update an user but sends no data', function (done) {
            chai.request(app)
                .put('/api/users')
                .set('x-access-token', token)                
                .send()
                .end(function(error, response){
                  response.should.have.status(400);
                  response.should.be.json;
                  done();
                });
        });
        it('Tries to change the username (which is not allowed)', function (done) {
            chai.request(app)
                .put('/api/users')
                .set('x-access-token', token)                
                .send({
                    "first_name": "diego ",
                    "last_name": " cardenas",
                    "passwrd": "123244114145",
                    "username": "123@gmail.gun"
                })
                .end(function(error, response){
                  response.should.have.status(400);
                  response.should.be.json;
                  done();
                });
        })
        it('updates an user succesfully', function (done) {
            let putUser = {
                "first_name": "fire",
                "last_name": "boi",
                "passwrd": "dismypass123"
            };
            chai.request(app)
                .put('/api/users')
                .set('x-access-token', token)                
                .send(putUser)
                .end(function(err, res){
                  if(err) done(err);
                  res.should.have.status(200);
                  res.should.be.json;
                  //console.log(res.body);
                  res.body.user.first_name.should.be.eq(putUser.first_name);
                  res.body.user.last_name.should.be.eq(putUser.last_name);
                  done();
                });
        })
    })
    describe('DELETES', function () {
        /* DELETE api/users/:id? */
        it('DELETEs the last user added (the one we previously added)', function(done) {
            chai.request(app)
                .delete('/api/users')
                .set('x-access-token', token)
                .end( function(err, res) {
                    if(err) done(err);
                    res.should.have.status(200);
                    res.should.be.json;
                    done();
                });
        });
        it('Tries to DElETE an user with an invalid token', function (done) {
            chai.request(app)
                .delete('/api/users/')
                .set('x-access-token', "tokeninvalid")
                .end(function(error, response){
                  response.should.have.status(401);
                  response.should.be.json;
                  done();
                });
        });
    })
});

describe ('API endpoint api/products', function () {
    let postedProduct;

    describe('POSTs', function () {
        it('POSTs a new product with admin access', function (done) {
            chai.request(app)
            .post('/api/products')
            .set('x-access-token', token)            
            .send({
                "name": "perrito",
                "description": "perrito de los buenos",
                "price": "4.20",
                "category_id": "1",
                "in_stock": "true"
            })
            .end( function(err, res) {
                if (err) {
                    console.log(err.message);
                    done(err);
                }
                res.should.have.status(200);
                res.should.be.json;
                res.body.product.should.have.ownProperty('name');
                res.body.product.should.have.ownProperty('description');
                res.body.product.should.have.ownProperty('price');
                res.body.product.should.have.ownProperty('category_id');
                res.body.product.should.have.ownProperty('in_stock');
                res.body.product.should.have.ownProperty('created');
                res.body.product.should.have.ownProperty('image_path');
                res.body.product.should.have.ownProperty('id');
                postedProduct = res.body.product;
                done();
            });
        });
        it('Fails to POST a new product due to non existent category',  (done) => {
            chai.request(app)
            .post('/api/products')
            .set('x-access-token', token)            
            .send({
                "name": "perrito",
                "description": "perrito de los buenos",
                "price": "4.20",
                "category_id": "13321531", // category doesnt exists
                "in_stock": "true"
            })
            .end( function(err, res) {
                res.should.have.status(422);
                res.should.be.json;
                done();
            });
        });
        it('Fails to POST a new product without admin access',  (done) => {
            chai.request(app)
            .post('/api/products')
            .set('x-access-token', token2)            
            .send({
                "name": "perrito",
                "description": "perrito de los buenos",
                "price": "4.20",
                "category_id": "1", // category doesnt exists
                "in_stock": "true"
            })
            .end( function(err, res) {
                res.should.have.status(401);
                res.should.be.json;
                done();
            });
        });
    })
    describe('GETs', function () {
        it('GETs all the products', function (done) {
            chai.request(app)
            .get('/api/products/all')
            .end( function(err, res) {
                if(err) done(err);
                res.statusCode.should.be.eq(200);
                let product = res.body.products[0];
                product.should.have.ownProperty('name');
                product.should.have.ownProperty('description');
                product.should.have.ownProperty('price');
                product.should.have.ownProperty('category_id');
                product.should.have.ownProperty('in_stock');
                product.should.have.ownProperty('created');
                product.should.have.ownProperty('image_path');
                product.should.have.ownProperty('id');
                done();
            })
        });
        it('GETs all the products in a category', function (done) {
            chai.request(app)
            .get('/api/products/bycategory_id/1')
            .end( function(err, res) {
                if(err) done(err);
                res.statusCode.should.be.eq(200);
                let product = res.body.products[0];
                product.should.have.ownProperty('name');
                product.should.have.ownProperty('description');
                product.should.have.ownProperty('price');
                product.should.have.ownProperty('category_id');
                product.should.have.ownProperty('in_stock');
                product.should.have.ownProperty('created');
                product.should.have.ownProperty('image_path');
                product.should.have.ownProperty('id');
                product.category_id.should.be.eq(1);
                done();
            })
        });
        it('Fails to GET products from an invalid category_id', function (done) {
            chai.request(app)
            .get('/api/products/bycategory_id/0')
            .end( function(err, res) {
                res.statusCode.should.be.eq(400);
                done();
            })
        });
        it('GETs an empty list of products from an non-existent but valid category_id', function (done) {
            chai.request(app)
            .get('/api/products/bycategory_id/266515120')
            .end( function(err, res) {
                res.statusCode.should.be.eq(200);
                res.body.should.have.ownProperty('products');
                res.body.products.should.be.an('array').that.is.empty;
                done();
            })
        });
        it('GETs a product by its ID', function (done) {
            chai.request(app)
            .get('/api/products/' + postedProduct.id)
            .end( function(err, res) {
                if(err) done(err);
                res.statusCode.should.be.eq(200);
                res.body.product.should.have.ownProperty('name');
                res.body.product.should.have.ownProperty('description');
                res.body.product.should.have.ownProperty('price');
                res.body.product.should.have.ownProperty('category_id');
                res.body.product.should.have.ownProperty('in_stock');
                res.body.product.should.have.ownProperty('created');
                res.body.product.should.have.ownProperty('image_path');
                res.body.product.should.have.ownProperty('id');
                done();
            })
        });
    });
    describe('PUTs', () => {
        it('Updates a product with admin access',  (done) => {
            chai.request(app)
            .put('/api/products/' + postedProduct.id)
            .set('x-access-token', token)            
            .send({
                "name": "Hot Doggity Dog",
                "in_stock": "false",
            })
            .end( function(err, res) {
                if (err) {
                    console.log(err.message);
                    done(err);
                }
                res.should.have.status(200);
                res.should.be.json;
                res.body.product.should.have.ownProperty('name');
                res.body.product.should.have.ownProperty('description');
                res.body.product.should.have.ownProperty('price');
                res.body.product.should.have.ownProperty('category_id');
                res.body.product.should.have.ownProperty('in_stock');
                res.body.product.should.have.ownProperty('created');
                res.body.product.should.have.ownProperty('image_path');
                res.body.product.should.have.ownProperty('id');
                done();
            });
        });
        it('Fails to update a product due non existen category',  (done) => {
            chai.request(app)
            .put('/api/products/' + postedProduct.id)
            .set('x-access-token', token)            
            .send({
                "category_id": "2298498",
            })
            .end( function(err, res) {
                res.should.have.status(422);
                res.should.be.json;
                done();
            });
        });
        it('Fails to update a product due to no admin access',  (done) => {
            chai.request(app)
            .put('/api/products/' + postedProduct.id)
            .set('x-access-token', token2)            
            .send({
                "category_id": "2298498",
            })
            .end( function(err, res) {
                res.should.have.status(401);
                res.should.be.json;
                done();
            });
        });
    });
    describe('DELETEs', () => {
        it('Deletes a product',  (done) => {
            chai.request(app)
            .delete('/api/products/' + postedProduct.id)
            .set('x-access-token', token)
            .end( function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                done();
            });
        });
        it('Fails to delete an already deleted product',  (done) => {
            chai.request(app)
            .delete('/api/products/' + postedProduct.id)
            .set('x-access-token', token)
            .end( function(err, res) {
                res.should.have.status(404);
                res.should.be.json;
                done();
            });
        });
        it('Fails to delete a product due to no admin access',  (done) => {
            chai.request(app)
            .delete('/api/products/' + postedProduct.id)
            .set('x-access-token', token2)
            .end( function(err, res) {
                res.should.have.status(401);
                res.should.be.json;
                done();
            });
        });
    });
})