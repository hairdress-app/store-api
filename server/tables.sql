/* postgreSQL */

CREATE TABLE users (
   id            SERIAL        NOT NULL PRIMARY KEY,
   created       DATE          DEFAULT NULL,
   first_name    VARCHAR(25)   NOT NULL,
   last_name     VARCHAR(25)   NOT NULL,
   username      VARCHAR(100)  NOT NULL,
   passwrd       VARCHAR(25)   NOT NULL,
   user_type_id  CHAR(1)       NOT NULL
)

CREATE TABLE products (
   id            SERIAL                  NOT NULL PRIMARY KEY,
   name          VARCHAR(100)            NOT NULL,
   description   VARCHAR                 NOT NULL,
   category_id   bigint                  NOT NULL,
   image         VARCHAR(255)            DEFAULT NULL,
   price         DOUBLE PRECISION        NOT NULL
)

CREATE TABLE categories (
    id           SERIAL                  NOT NULL PRIMARY KEY,
    name         VARCHAR(100)            NOT NULL,
    image        VARCHAR                 DEFAULT NULL
)

-- Todo: passwrd should be 100 length in heroku