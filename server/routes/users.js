'use strict'
var express = require('express')
var router = express.Router()

var users = require('../models/users')

/* GET all users */
router.get('/all', users.TOKEN_SCHEMA, users.getAll)

/* GET single user */
router.get('/', users.TOKEN_SCHEMA, users.getSingle)

/* create user */
router.post('/', users.POST_SCHEMA, users.create)

/* modify user */
router.put('/', users.PUT_SCHEMA, users.update)

/* delete user */
router.delete('/', users.TOKEN_SCHEMA, users.removeById)

/* compares the username and passwrd passed to the ones in the database
   and returns a JWT if they match */
router.post('/auth', users.AUTH_SCHEMA, users.auth)

module.exports = router
