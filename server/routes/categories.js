'use strict'
var express = require('express')
var router = express.Router()

var categories = require('../models/categories')

/* GET all categories */
router.get('/', categories.getAll)

module.exports = router
