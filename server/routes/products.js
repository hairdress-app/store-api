'use strict';
var express = require('express');
var router = express.Router();

var products = require('../models/products');

/* GET all products */
router.get('/all', products.getAll);

/* GET single product */
router.get('/:id?', products.REQUIRE_ID_SCHEMA, products.getById);

/* GET single product */
router.get('/bycategory_id/:category_id?', products.GET_BY_CATEGORY_SCHEMA, products.getByCategoryId);

/* POST create product */
router.post('/', products.CREATE_SCHEMA, products.create);

/* Update product */
router.put('/:id?', products.PUT_SCHEMA, products.update);

/* delete product */
router.delete('/:id?', products.DELETE_BY_ID_SCHEMA, products.removeById);


module.exports = router;
