const usernameInUse = 'Username already in use';
  
const opNotCompleted = 'The operation could not be completed.';

//const notFound = 'The operation could not be completed.';

const userUnauthorized = 'Authentication failed. User not found.';

// exports the variables and functions above so that other modules can use them
module.exports.userUnauthorized = userUnauthorized;
//module.exports.notFound = notFound;
//module.exports.badRequest = badRequest;
//module.exports.unprocesableEntity = unprocesableEntity;