const { Nuxt, Builder } = require('nuxt')
var express = require('express')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var cors = require('cors')

// var index = require('./routes/index')
var users = require('./routes/users')
var products = require('./routes/products')
var categories = require('./routes/categories')

var app = express()

app.use(cors())

// Dont log requests on tests
if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'))
}

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
// app.use(express.static(path.join(__dirname, 'public')))

// app.use('/', index)
app.use('/api/users', users)
app.use('/api/products', products)
app.use('/api/categories', categories)

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

// Init Nuxt.js
const nuxt = new Nuxt(config)

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt)
  builder.build()
}

// Give nuxt middleware to express
app.use(nuxt.render)

// development error handler
// will print stacktrace
if (app.get('env') !== 'production') {
  app.use(function (err, req, res, next) {
    if (err.isJoi) {
      err.status = 400
    }
    res.status(err.status || 500)
      .json({
        status: 'error',
        message: err
      })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  if (err.isJoi) {
    err.status = 400
  }
  // I think err.status should be err.code
  res.status(err.status || 500)
    .json({
      status: err.status,
      message: err.message
    })
})

module.exports = app
