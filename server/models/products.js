'use strict'
const db = require('../dbconnection')
const appError = require('../utils/appError')
const jwt = require('jsonwebtoken')
const { celebrate, Joi, errors } = require('celebrate')

// requires JWT
const REQUIRE_TOKEN_SCHEMA = {
  headers: Joi.object().keys({
    'x-access-token': Joi.string().required()
  }).unknown()
}

const CREATE_SCHEMA = {
  body: Joi.object().keys({
    name: Joi.string().min(2).max(100).trim().required(),
    description: Joi.string().max(2048).trim().default(''),
    in_stock: Joi.boolean().truthy(['yes', 1]).falsy(['no', 0]).default(true),
    image_path: Joi.string().min(3).max(255).trim().default(''),
    price: Joi.number().required().default('1'),
    category_id: Joi.number().integer().greater(0).default(1)
  })
}

const PUT_SCHEMA = {
  body: Joi.object().keys({
    name: Joi.string().min(2).max(100).trim(),
    description: Joi.string().max(2048).trim(),
    in_stock: Joi.boolean().truthy(['yes', 1]).falsy(['no', 0]),
    image_path: Joi.string().min(3).max(255).trim(),
    price: Joi.number().greater(0),
    category_id: Joi.number().integer().greater(0)
  }),
  headers: REQUIRE_TOKEN_SCHEMA.headers
}

const GET_BY_CATEGORY_SCHEMA = {
  params: Joi.object().keys({
    category_id: Joi.number().greater(0).required()
  })
}

const REQUIRE_ID_SCHEMA = {
  params: Joi.object().keys({
    id: Joi.number().greater(0).required()
  })
}

const DELETE_BY_ID_SCHEMA = {
  params: REQUIRE_ID_SCHEMA.params,
  headers: REQUIRE_TOKEN_SCHEMA.headers
}

module.exports = {
  getAll: getAll,
  getById: getById,
  create: create,
  getByCategoryId: getByCategoryId,
  update: update,
  removeById: removeById,
  CREATE_SCHEMA: celebrate(CREATE_SCHEMA),
  REQUIRE_TOKEN_SCHEMA: celebrate(REQUIRE_TOKEN_SCHEMA),
  PUT_SCHEMA: celebrate(PUT_SCHEMA),
  GET_BY_CATEGORY_SCHEMA: celebrate(GET_BY_CATEGORY_SCHEMA),
  DELETE_BY_ID_SCHEMA: celebrate(DELETE_BY_ID_SCHEMA),
  REQUIRE_ID_SCHEMA: celebrate(REQUIRE_ID_SCHEMA)
}

// Todo: ask for token
function create (req, res, next) {
  let token = req.headers['x-access-token']

  // validate token
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    // console.log(decoded);
    if (decoded.user_type_id != 0 || err) return res.status(401).send({ auth: false, message: 'User unauthorized for this action' })

    req.body.created = new Date().toISOString().split('T')[0]

    // since category_id is a foreing key we need to check that it exists
    getCategoryById(req.body.category_id)
      .then((category) => {
        if (category == null) {
          return next(new appError('The category specified on the product does not exist', 422))
        }

        // console.log(category + 'wat')

        // Insert the new product and returns its data
        db.one(
          'insert into products(created, name, description, image_path, price, category_id, in_stock)' +
        'values(${created}, ${name}, ${description}, ${image_path}, ${price}, ${category_id}, ${in_stock})' +
        ' RETURNING * ',
          req.body)
          .then(function (product) {
            res.status(200)
              .json({
                success: 'New product created',
                product: product
              })
          })
          .catch(function (err) {
            console.log('error on the create product promise')
            console.log(err)
            return next(err)
          })
      })
      .catch((err) => {
        console.log(err)
        return next(new appError('Something went wrong on GetCategoryById', 500))
      })
  })
}
function getAll (req, res, next) {
  db.any('select * from products order by id')
    .then(function (products) {
      res.status(200)
        .json(products)
    })
    .catch(function (err) {
      console.log('error on the get all products promise')
      console.log(err)
      return next(err)
    })
}

function getById (req, res, next) {
  getProductById(req.params.id)
    .then(function (product) {
      res.status(200)
        .json({product: product})
    })
    .catch(function (err) {
      console.log('error on the get product by Id promise')
      console.log(err)
      return next(err)
    })
}

function getByCategoryId (req, res, next) {
  db.any('select * from products where category_id = $1', req.params.category_id)
    .then(function (products) {
      res.status(200)
        .json({products: products})
    })
    .catch(function (err) {
      console.log('error on the get products by category promise')
      console.log(err)
      return next(err)
    })
}

// Todo: ask for token
function update (req, res, next) {
  let token = req.headers['x-access-token']

  // validate token
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    // console.log(decoded);
    if (decoded.user_type_id != 0 || err) return res.status(401).send({ auth: false, message: 'User unauthorized for this action' })

    // prove that the product exists
    getProductById(req.params.id)
      .then(function (product) {
      // console.log(req.body);
      /* we dont want to overwrite the whole product
      but only the data given in the request */
        Object.entries(req.body).forEach(([key, value]) => {
          product[key] = value
        })
        // console.log(product);
        // if it doesn't exists throw 404
        if (product === null) return next(new appError('The product could not be found, maybe it doesnt exists', 404))
        // if it does exists update with the data provided and return the updated product
        db.one('update products set name=${name}, description=${description}, image_path=${image_path}, price=${price}, category_id=${category_id}, in_stock=${in_stock} WHERE id=${id} RETURNING *',
          product)
          .then(function (productUpdated) {
            res.status(200)
              .json({
                status: 'success',
                message: 'Product updated',
                product: productUpdated
              })
          })
          .catch(function (err) {
            if (err.code == '23503') {
              err = new appError('The category passed does not exists', 422)
            } else {
              console.log('error updating the product')
              console.log(err)
            }
            return next(err)
          })
      })
      .catch(function (err) {
        console.log('error finding the product on the update product by Id promise')
        console.log(err)
        return next(err)
      })
  })
}

function removeById (req, res, next) {
  let token = req.headers['x-access-token']

  // validate token
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    // console.log(decoded);
    if (decoded.user_type_id != 0 || err) return res.status(401).send({ auth: false, message: 'User unauthorized for this action' })

    // delete the product
    db.result('delete from products where id = $1', req.params.id)
      .then(function (result) {
        if (result.rowCount > 0) {
          res.status(200)
            .json({
              status: 'success',
              message: `Removed ${result.rowCount} product`
            })
        } else {
          return next(new appError('The product to delete was not found or it does not exists', 404))
        }
      })
      .catch((err) => {
        console.log(err)
        return next(err)
      })
  })
}

/// / DB Querys
function getProductById (id) {
  return db.oneOrNone('select * from products where id = $1', id)
}

function getCategoryById (id) {
  return db.oneOrNone('select * from categories where id = $1', id)
}
