'use strict'
const db = require('../dbconnection')
const appError = require('../utils/appError')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { celebrate, Joi, errors } = require('celebrate')

// requires JWT
const TOKEN_SCHEMA = {
  headers: Joi.object({
    'x-access-token': Joi.string().required()
  }).unknown()
}

const POST_SCHEMA = {
  body: Joi.object().keys({
    first_name: Joi.string().min(3).max(25).trim().required(),
    last_name: Joi.string().min(3).max(25).trim().required(),
    username: Joi.string().trim().email().required(),
    passwrd: Joi.string().min(6).max(100).regex(/\d/).required(),
    user_type_id: Joi.number().default('1')
  })
}

const PUT_SCHEMA = {
  body: Joi.object().keys({
    first_name: Joi.string().min(3).max(25).trim().required(),
    last_name: Joi.string().min(3).max(25).trim().required(),
    passwrd: Joi.string().min(6).max(100).regex(/\d/).required()
  }),
  headers: Joi.object({
    'x-access-token': Joi.string().required()
  }).unknown()
}

const AUTH_SCHEMA = {
  body: Joi.object().keys({
    username: Joi.string().trim().email().required(),
    passwrd: Joi.string().min(6).max(100).regex(/\d/).required()
  })
}

module.exports = {
  getAll: getAll,
  getSingle: getSingle,
  create: create,
  update: update,
  removeById: removeById,
  auth: auth,
  POST_SCHEMA: celebrate(POST_SCHEMA),
  TOKEN_SCHEMA: celebrate(TOKEN_SCHEMA),
  PUT_SCHEMA: celebrate(PUT_SCHEMA),
  AUTH_SCHEMA: celebrate(AUTH_SCHEMA)
}

/* Responds with all the users in the db or throws an error */
function getAll (req, res, next) {
  let token = req.headers['x-access-token']
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    if (err || decoded.user_type_id != 0) return res.status(401).send({ auth: false, message: 'Unauthorized operation.' })

    // console.log(decoded.user_type_id)
    db.any('select id, first_name, last_name, username, created from users order by id')
      .then(function (users) {
        res.status(200)
          .json(users)
      })
      .catch(function (err) {
        return next(err)
      })
  })
}

/* Finds a user by its id and responds the request with the users data;
   Throws error 404 if the user was not found */
function getSingle (req, res, next) {
  // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  let token = req.headers['x-access-token']
  //console.log(req.headers);
  //console.log("passed validation");
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    if (err) return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' })

    getById(decoded.id)
      .then(function (user) {
      // console.log(user)
        if (user === null) {
          return next(new appError('The user was not found or it doesnt exists', 404))
      } else {
          res.status(200)
            .json({
              status: 'success',
              data: user,
              message: 'Retrieved ONE user'
            })
      }
      })
  })
}

function create (req, res, next) {
  // console.log("llego")
  usernameInUse(req.body.username)
    .then(emailExists => {
      // console.log(emailExists);
      /* A user with that username already exists return error
         emailExists comes as an array of with a bool (ie [true]) */
      if (emailExists.pop()) {
        return next(new appError('Email already exists', 422))
      } else {
        // console.log(req.body);
        // Date of creation
        req.body.created = new Date().toISOString().split('T')[0]
        let hashed_pass = bcrypt.hashSync(req.body.passwrd, 10)
        //console.log(hashed_pass);
        req.body.passwrd = hashed_pass

        // finally register the user on the db and returns its ID
        db.one('insert into users(created, first_name, last_name, username, passwrd, user_type_id)' +
          'values(${created}, ${first_name}, ${last_name}, ${username}, ${passwrd}, ${user_type_id}) RETURNING id, user_type_id',
        req.body)
          .then(function (user) {
            // console.log(user);
            // console.log(process.env.JWT_SECRET);
            // create a token
            let token = jwt.sign({ id: user.id, user_type_id: user.user_type_id },
              process.env.JWT_SECRET,
              { expiresIn: 86400 }
            )
            res.status(200)
              .json({
                auth: true,
                token: token
              })
          })
          .catch(function (err) {
            console.log('error on the create user promise')
            return next(err)
          })
      }
    })
}

/* updates the 'first_name', 'last_name' and 'password' of an user by its id */
function update (req, res, next) {
  let token = req.headers['x-access-token']

  // validate token
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    // console.log(token);
    if (err) return res.status(401).send({ auth: false, message: 'Invalid token.' })


    // checks against the db if the user exists
    getById(decoded.id)
      .then(function (user) {
        // console.log(user)
        // if the user doesn't exists throw an errorn
        if (user === null) {
          return next(new appError('The user was not found or it doesnt exists', 404))
        // if the user exists update it on the db with the new data
        } else {
          // encrypt the password first
          req.body.passwrd = bcrypt.hashSync(req.body.passwrd, 10)
          db.one('update users set first_name=$1, last_name=$2, passwrd=$3 where id=$4 RETURNING created, first_name, last_name, username, user_type_id',
            [req.body.first_name, req.body.last_name, req.body.passwrd, decoded.id])
            .then(function (user) {
              // console.log(user);
              res.status(200)
                .json({
                  status: 'success',
                  message: 'User updated',
                  user: user
                })
            })
            .catch(function (err) {
              return next(err)
            })
        }
      })
  })
}

/* removes a single user with its token */
function removeById (req, res, next) {
  let token = req.headers['x-access-token']

  // validate token
  jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    // console.log(token);
    if (err) return res.status(401).send({ auth: false, message: 'Invalid token.' })

    // delete the user
    db.result('delete from users where id = $1', decoded.id)
      .then(function (result) {
        if (result.rowCount == 1) {
          res.status(200)
            .json({
              status: 'success',
              message: `Removed ${result.rowCount} user`
            })
        } else {
          return next(new appError('The user was not found or it doesnt exists', 404))
        }
      })
  })
}

function auth (req, res, next) {
  getUserByUsername(req.body.username)
    .then(user => {
      if (user === null) {
        return next(new appError('User authentication failed. ', 401))
      } else {
        // Load hash from your password DB.
        bcrypt.compare(req.body.passwrd, user.passwrd, function (err, passwordMatched) {
          // console.log(passwordMatched);
          // res == true
          if (!passwordMatched) {
            return next(new appError('User authentication failed', 401))
              } else {
            // console.log(req.body);
            let token = jwt.sign({ id: user.id, user_type_id: user.user_type_id },
              process.env.JWT_SECRET,
              { expiresIn: 86400 }
            )
                  res.status(200)
              .json({
                status: 'success',
                token: token
              })    
              }
        })


      }
    })
}

/* this function return a promise with the user data
 if the user's credentials is valid, else return Null */
function getUserByUsername (email) {
  return db.oneOrNone('SELECT id, created, first_name, last_name, username, passwrd, user_type_id FROM users WHERE username = $1', email)
}

/* returns a promise with true if the email is already in use
   looks like the map function returns the value as [true] or [false] (inside an array) */
function usernameInUse (email) {
  return db.map('select count(*) from users where username = $1', email,
    data => data.count != 0)
}

/* This function returns a promise for the user data */
function getById (id) {
  return db.oneOrNone('select id, first_name, last_name, username, created from users where id = $1', id)
}
