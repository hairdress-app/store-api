'use strict'
const db = require('../dbconnection')
const appError = require('../utils/appError')

module.exports = {
  getAll: getAll
}

/* Gets all the categories from the db */
function getAll (req, res, next) {
  // console.log(decoded.user_type_id)
  db.any('select * from categories order by id')
    .then(function (users) {
      res.status(200)
        .json(users)
    })
    .catch(function (err) {
      console.log(err)
      return next(new appError('Something went wrong querying the db for categories', 500))
    })
}
